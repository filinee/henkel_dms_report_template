//var json = require('./data/validation.json');

/*
 * Обработка и возвращение данных
 *
 * */
var general = function () {
        
        var that = this;

        this.generalData = generalData;

        this.project_ver = project_ver;

        this.dataChanges = dataChanges;

        this.dataQuality = dataQuality;

        this.dataVsTotal = dataVsTotal;

        this.dataErrorDescr = dataErrorDescr;

        this.dataByDocType = dataByDocType;

        this.regContol = reglamentController;

        /*
         * Возвращает всего обработанных объектов в разрезе месяцев
         * */
        this.getTotalByMonth = function () {
           var data = [];
           for (var el in that.generalData) {
                data.push([that.generalData[el]['month'], parseInt(that.generalData[el]['total_objects'], 10)]);
           }
           return data;
        };

        /*
         * Возвращает количество новых объектов и старых в разрезе месяцев 
         * */
        this.getOldNewObjByMonth = function () {
           var data1_old = [];
           var data2_new = [];
           for (var el in that.generalData) {
                data1_old.push([that.generalData[el]['month'], parseInt(that.generalData[el]['old_objects'], 10)]);
                data2_new.push([that.generalData[el]['month'], parseInt(that.generalData[el]['new_objects'], 10)]);
           }
           return [data1_old,data2_new];
        };

        /*
         * Возвращает количество ошибок в данных по месяцам 
         * */

        this.getErrorsByMonth = function () {
           var data = [];
           for (var el in that.generalData) {
                data.push([that.generalData[el]['month'], parseInt(that.generalData[el]['error_objects'], 10)]);
           }
           return data;
        };

        /*
         * Возвращает количество ошибок и общее количество документов в разрезе типов документов (продажи, возвраты и тд)
         * */
        this.getInfoByDoc = function () {
            var data = {};
           for (var el in that.dataByDocType) {

               var docId = that.dataByDocType[el]['doctypeid'];

               if (typeof data[docId] == typeof undefined) {
                   data[docId] = {error: 0, count: 0};
               }
               var error = parseInt(that.dataByDocType[el]['error'], 10); 
               var count = parseInt(that.dataByDocType[el]['count'], 10); 

               data[docId]['error'] += error > 0? error: 0;
               data[docId]['count'] += count > 0? count: 0;
                
           }
            return data; 
        };

        /*
         * Возвращает информацию по типам ошибок и их количестве 
         * */
        
        this.getDataErrorDescr = function () {
            return that.dataErrorDescr;
        };

        /*
         * Возвращает количество ошибок, новых элементов и старых элементов 
         * */
        this.getErrorVsTotal = function () {
            var data = {error:0, new_items:0, old: 0};

           for (var el in that.dataVsTotal) {

               var error = parseFloat(that.dataVsTotal[el]['errors'], 10); 
               var new_items = parseFloat(that.dataVsTotal[el]['new'], 10); 
               var old = parseFloat(that.dataVsTotal[el]['old'], 10); 

               data['error'] += error > 0? error: 0;
               data['new_items'] += new_items > 0? new_items: 0;
               data['old'] += old > 0? old: 0;
                
           }
            return data; 
        };


        /*
         * Возвращает количество документов каждого типа (продажи, возвраты), в разрезе месяцев 
         * */
        this.getDataQByMonth = function () {
            var data = {};
            for (var el in that.dataQuality) {

                var month = that.dataQuality[el]['month'];
                var doctype = that.dataQuality[el]['doctypeid'];
                var count = that.dataQuality[el]['count'];

                if (typeof data[doctype] == typeof undefined) {
                    data[doctype] = [];
                }

               data[doctype].push([month, count]);
                
           }
            return data; 

        };
        

        /*
         * Возвращает количество изменений по месцам 
         * */
        this.getChanges = function () {
           var data = [];
           for (var el in that.dataChanges) {
               data.push([that.dataChanges[el]['month'], that.dataChanges[el]['count']]);
           }
            return data; 

        };

        this.getRegContol = function () {
           var data = [];
           for (var el in that.regContol) {
               data.push([that.regContol[el]['month'], that.regContol[el]['resource'], that.regContol[el]['text'],that.regContol[el]['count']]);
           }
            return data; 

        };
        this.getRegContolByMonth = function () {
           var data = [];
           var month_count = {};
           var month, count;
           for (var el in that.regContol) {

               month = that.regContol[el]['month'];
               count = parseInt(that.regContol[el]['count'], 10);

               if (typeof month_count[month]  == typeof undefined) {
                    month_count[month] = 0;
                }
               month_count[month] += count; 
               //data.push([that.regContol[el]['month'],parseInt(that.regContol[el]['count'], 10)]);
           }
           
            for (var el in month_count) {
                data.push([el, month_count[el]]);
            }

            return data; 

        };
        this.getRegContolByType = function () {
           var data = [];
           var type_count = {};
           var type, count;
           for (var el in that.regContol) {

               type = that.regContol[el]['text'];
               count = parseInt(that.regContol[el]['count'], 10);

               if (typeof type_count[type]  == typeof undefined) {
                    type_count[type] = 0;
                }
               type_count[type] += count; 
               //data.push([that.regContol[el]['month'],parseInt(that.regContol[el]['count'], 10)]);
           }
           
           var i = 0;
           var data_names = [];
            for (var el in type_count) {
                i += 1;
                data.push([type_count[el], i]);
                data_names.push([i, el]);
            }

            return {'data': data, 'data_names': data_names}; 

        };
        /*
         * Возвращает информацию о версии отчета  
         * */
        this.getProjectVer = function () {
            return that.project_ver;
        };

};


/*
 * Группа обработчиков angularjs
 */
var henkelReport = angular.module('henkelReport', ['duScroll', 'datatables']);

henkelReport.filter('addZero', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });

henkelReport.controller('menuCtrl', function ($scope) {

    //Определем, какие размеры должны быть у графиков в зависимости от их положения
    $scope.plot_type = function (el_id, plot_num) {
        var ret = 'plot';

        //Действие по умолчанию - оставлям класс plot
        if (el_id == 'menu1') {
            ret = 'plot';
        //Размеры для круговой диаграммы
        } else if (el_id == 'menu2') {
            ret = 'plotPie'; 
        //Двойная ширина для столбчатой диаграммы
        } else if (el_id == 'menu3') {
            ret = 'plot2';
        } else if (el_id == 'menu4') {
            ret = 'plot2';
        }

        return ret;

    };

    //Информация для формированию меню и спика обработок
    $scope.menu_elements = [
        {header:"Общая информация", id:'menu1', table_header: "Общая информация по документам"},
        {header:"Ошибки в документах", id:'menu2',  table_header: "Информация по ошибкам"},
        {header:"Изменения в документах", id:'menu3',  table_header: "Информация по изменениям в документах"},
        {header:"Выгрузка документов", id:'menu4',  table_header: "Информация по полноте выгрузки"},
        {header:"Несоблюдение регламента", id:'menu5',  table_header: "Несоблюдение регламента"}
    ];

});

/*
 * Фабрика предоставляющая объект с настройками всех таблиц кроме дополнительной
 * */
henkelReport.factory('getTableObj', function ()  {

    var g = new general;

    var infoQuality = g.getDataQByMonth();

    var tablesObj = {
        table_general: 
            { 
                columns:  [{colName:"Тип документа"}, {colName:"Корректных объектов"}, {colName:"Ошибки"}],
                data:  g.getInfoByDoc(),
                add_file_type: 'vl',
                add_json_field : 1, 
                add_text_search: 0
            },
        table_error: {
                columns:  [{colName:"Тип ошибки"}, {colName:"Количество"}],
                data:  g.getDataErrorDescr(),
                add_file_type: 'vl',
                add_json_field : 9, 
                add_text_search: 0
        },
        table_changes: {
                columns:  [{colName:"Месяц"}, {colName:"Количество изменений"}],
                data:   g.getChanges(),
                add_file_type: 'pt',
                add_json_field : -1, 
                add_text_search: -1
        },
        table_quality: {
                columns:  [{colName:"Месяц"}, {colName:"Тип документа"}, {colName:"Выгружено"}],
                data:  getInfoQuality(),
                add_file_type: 'il',
                add_json_field : -1, 
                add_text_search: -1
        },

        table_schedule: {
            columns:  [{colName:"Месяц"}, {colName:"Источник"}, {colName:"Название"}, {colName:"Количество"}],
                data:  g.getRegContol(),
                add_file_type: 'rc',
                add_json_field : 6, 
                add_text_search: 2
        }

    };



    //Преобразует трехмерный массив в двумерный
    function getInfoQuality() {
        var ret = [];
        var monthIndex = 0;
        var countIndex = 1;
        for (var doctype in infoQuality) {

            for (var i in infoQuality[doctype]) {

                var tmp = {};

                tmp['month'] = infoQuality[doctype][i][monthIndex];
                tmp['doctypeid'] = doctype;
                tmp['count'] = infoQuality[doctype][i][countIndex];

                ret.push(tmp);
            }

        }
        return ret;
    }
    return tablesObj;

});

/* 
 * Фабрика предоставляющая объекты для работы с сводной таблицей 
 * */
henkelReport.factory('pivotFactory', function ()  {

    obj = {
         pivottableid: '#pivottable',
         pivottable_dataid: '#pivottable_data',
         showPivotTable: false,
         open_pivot_table: function () {

            var wh = $(window).height();   // returns height of browser viewport
            var ww = $(window).width();   // returns width of browser viewport
            $(this.pivottableid).css({'width': (ww - 25) + 'px', 'height': (wh - 25) + 'px'});

             this.showPivotTable = true
         },

         hide_pivot_table: function () {this.showPivotTable = false},
         add_pivot_data: function (type) {

             var data = window[file_names[type]['name']];
             $.pivotUtilities.data = data;

             var settings = {};

             if (type == 'rc') {
                settings = {
                    rows: ["Ресурс данных"],
                    cols: ["Год","Месяц", "День"],
                    vals: ["Была выгрузка данных"],
                    aggregatorName: "Sum",
                    //renderers: $.extend( $.pivotUtilities.renderers),
                    rendererName: "Heatmap_green"
                };
             } else {
                settings = {
                    rows: ["Описание"],
                    cols: ["Год","Месяц", "День"],
                    vals: ["Была выгрузка данных"],
                    aggregatorName: "Sum",
                    //renderers: $.extend( $.pivotUtilities.renderers),
                    rendererName: "Heatmap_green"
                };

             }

             $(this.pivottable_dataid).pivotUI(
                     $.pivotUtilities.data,
                     settings
                );           
         }
    };

    return obj;

});

/*
 * Контроллер отображающий сводную таблицу 
 */
henkelReport.controller('pivotCtrl', function ($scope, $rootScope, $window, pivotFactory) {
    //$(document).height(); // returns height of HTML document
    //$(document).width(); // returns width of HTML document
    $scope.pt = pivotFactory;


   $scope.open_pivot_table = function () {
        console.log('pivot');
   }; 
});

/*
 * Контроллер для вывода заголовков и данных в таблицы
 */
henkelReport.controller('tableCtrl', function ($scope, $rootScope, $window, getTableObj) {
      

    $scope.tablesObj = getTableObj;
    console.log ($scope.tablesObj);

    $scope.month_name = $window.month_name;
    $scope.doc_type = $window.doc_type;

    //$scope.month = 'month';

    //$scope.updateExtraTable = updateExtraTable;

    //Ф-я показывает 
    $scope.show_general = function (e) {
        $rootScope.showAddInfo = true;
        
        //Скрываем кнопку идти в начало страницы
        $scope.go_to_top_button = angular.element('.go_top');
        $scope.go_to_top_button.stop(true, true).fadeOut();

        //Элемент куда мы нажали
        var target = e.currentTarget;

        // Тип таблицы на которую нажали
        var targetTable = angular.element(target).parent().parent().attr('id');
        console.log (target);

        //Индекс столбца в таблице, откуда мы возмем значения для поиска в основном массиве
        var el_to_find = $scope.tablesObj[targetTable]['add_text_search'];

        //С каким полем сравниваем в основном массиве
        var el_in_arr = $scope.tablesObj[targetTable]['add_json_field'];

        //С каким типом файла мы имеем дело
        var filetype = $scope.tablesObj[targetTable]['add_file_type'];

        //Получаем переменную в которой хранятся наши данные
        var addData = $window[file_names[filetype]['name']];

        //$rootScope.dataToShow = addData.slice(1, addData.length);
       // 
        var targetVal_text = angular.element(target).find('td').eq(el_to_find).text();
        var targetVal_search = angular.element(target).find('td').eq(el_to_find).attr('data-search');

        var targetVal = typeof targetVal_search === typeof undefined? targetVal_text: targetVal_search;
        console.log (targetVal_search);
        var arr_length = addData.length;


        //Цикл по всем элементам массива и выборка только нужных нам, на основании строки на которой кликнули
        var tmp_arr=[];
        
        if (el_in_arr === -1 || el_to_find === -1) {
            
            tmp_arr = addData.slice(1, addData.length);

        } else {

            for (var i=1; i < arr_length; i++) {
                if (addData[i][el_in_arr] == targetVal) {
                    tmp_arr.push(addData[i]);
                }
            }
        }
        //Формируем шапку
        $rootScope.dataToShowHead = addData[0];

        //Показываем данные 
        $rootScope.dataToShow = tmp_arr;//addData.slice(1, addData.length);
        
        //Увеличиваем количество элементов для просмотра
        $rootScope.limitToShowRecords = Math.min($rootScope.dataToShow.length, $rootScope.limitToShowRecords);

    };
});

/*
 * Контроллер для вывода информации о версии приложения, даты начала и конца периода отчета 
 */
henkelReport.controller('verCtrl', function ($scope) {
        
    var g = new general;

    var project_ver = g.getProjectVer();

    $scope.columns_ver = [{colName:"Дата фомирования"}, {colName:"Начало периода"}, {colName:"Конец периода"}, {colName:"Версия"}];
    $scope.versionInfo = project_ver;

});

/*
Показ раздела с дополнительной информацией
 */
henkelReport.controller('getAddDataCtrl', function ($scope, $rootScope, DTOptionsBuilder, DTColumnBuilder) {
   // Инициация настроек datatable 
   $scope.dtOptions = DTOptionsBuilder.newOptions();

   // Сбрасываем значения объектов datatable 
   $scope.dtInstances = [];
   $scope.dtInstance = {};

    // Скрыть раздел с доп информацией 
   $rootScope.showAddInfo = false; 

   // Ф-я скрытия раздела с доп информацией 
   $scope.hideAddInfo = function () {
       $rootScope.showAddInfo = false;
   };

   //Ограничение количества строк для показа
   $rootScope.limitToShowRecords = 100;

   // Заполняем таблицу "пустыми" значениями
   var addData = [['Нет заголовка'],[['Нет данных']]];
   // Выделяем данные из пустого массива 
   $rootScope.dataToShow = addData[1];
   // Выделяем заголовок из пустого массива
   $rootScope.dataToShowHead = addData[0];

   //Обработчик клика на кнопке "Загрузить еще"
   $scope.loadMore = function () {
        if ($rootScope.dataToShow.length <= $rootScope.limitToShowRecords) {
            $rootScope.limitToShowRecords = $rootScope.dataToShow.length; 
            window.alert("Все записи загружены");
            return true;
        } else {
            $rootScope.limitToShowRecords += 100; 
        }
    };
});

/*
 * Функция формированию всех графиков
*/
drawPlots = function () {

    //Формат меток для круговой диаграммы
    function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:black;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    }

    var g = new general;

    /*
     * Данные для общей информации
     */
    var data_totalByMonth = g.getTotalByMonth();
    var data_errorByMont = g.getErrorsByMonth();
    var data_oldNew = g.getOldNewObjByMonth();

    var placeholder = $('.plot.plot_num_1.menu1');

    $.plot(placeholder, [{label:"Кол-о элементов", data:data_totalByMonth}], {
        series: {
            bars: {
                show: true,
                barWidth: 0.6,
                lineWidth: 0,
                align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });

    var placeholder = $('.plot.plot_num_2.menu1');
    $.plot(placeholder, [{label:"Количество ошибок", data:data_errorByMont}], {
        series: {
            color:'red',
        bars: {
            show: true,
            barWidth: 0.6,
            lineWidth: 0,
            align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });


    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    var placeholder = $('.plot.plot_num_3.menu1');
    $.plot(placeholder, [{label:'Без изменений', data:data_oldNew[0]}, {label:'Измененные', data:data_oldNew[1]}], {
        series: {
            stack: 1,
        lines: {
            show: lines,
            fill: true,
            steps: steps
        },
        bars: {
            lineWidth: 0,
            show: bars,
            barWidth: 0.6
        }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });

    var data_errorVsTotal = g.getErrorVsTotal();
    var placeholder = $('.plotPie.plot_num_1.menu2');
    $.plot(placeholder, [{label:'Ошибки', data:data_errorVsTotal['error']}, {label:'Без изменений', data:data_errorVsTotal['old']}, {label:'Новые', data:data_errorVsTotal['new_items']}], {
        series: {
            pie: { 
                show: true,
            radius: 2/3,
            label: {
                show: true,
                radius: 2/3,
                formatter: labelFormatter,
                background: { 
                    opacity: 0.5
                }
            }
            }
        },
        legend: {
            show: false
        }
    });

    var getDataQByMonth = g.getDataQByMonth();
    var placeholder = $('.plot2.plot_num_1.menu4');
    $.plot(placeholder, [{label:"Продажи", data:getDataQByMonth[2], bars:{order:0,  show: true, barWidth:0.22, lineWidth: 0}}, {label:"Возвраты", data:getDataQByMonth[9], color:'green', bars:{order:1, show:true, barWidth:0.22, lineWidth: 0}}], {
        bars: {
            lineWidth: 0
        },
        series: {
            barWidth:0.01
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    
    });

    var changes = g.getChanges();
    console.log(changes);
    var placeholder = $('.plot2.plot_num_1.menu3');
    $.plot(placeholder, [{label:"Изменения по месяцам", data:changes, bars:{show: true, barWidth:0.22, lineWidth: 0}} ], {
        series: {
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });

    var placeholder = $('.plot.plot_num_2.menu5');

    var reglament_by_month = g.getRegContolByMonth();
    var reglament_by_type = g.getRegContolByType();
    console.log(reglament_by_month);
    console.log(reglament_by_type);
    $.plot(placeholder, [{label:"Ошибок регламета по мес.", data:reglament_by_month}], {
        series: {
            bars: {
                show: true,
                barWidth: 0.6,
                lineWidth: 0,
                align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });


    var placeholder = $('.plot_num_4.menu5');
    $.plot(placeholder, [{ label:"Несоблюдение регламента по типам документов", data:reglament_by_type['data']}], {
        series: {
            bars: {
                show: true,
                barWidth: 0.6,
                lineWidth: 0,
                align: "center",
                horizontal: true
            }
        },
        xaxis: {
            axisLabel: "Ошибок",
            //tickLength: 0
        },
        yaxis: {
            axisLabel: "Типы документов",
            //mode: "categories",
            ticks: reglament_by_type['data_names'],
            tickLength: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderWidth:0
        },
        tooltip: {
            show: true,
            content: '%y',
            shifts: {
                x: 20,
                y: -20
            }

        }
    });


    /*
     *
     for (var el in data) {

     var placeholder = $( '[data-sess_id="' + data[el]["data-sess_id"] +'"] [data-plot_id="' + data[el]["plot-pos"]  + '"]' );

     placeholder.unbind();

     $.plot(placeholder, data[el].flotdata, {
     series: {
     pie: { 
     show: true,
     radius: 2/3,
     label: {
     show: true,
     radius: 2/3,
     formatter: labelFormatter,
     background: { 
     opacity: 0.5
     }
     }
     }
     },
     legend: {
     show: false
     }
     });
     }

*/





}; 

/*
 * Убирает а затем добавляет data table к таблице с дополнительными данными
 */
function updateExtraTable() {
        $('#addInfoTable').DataTable({
        "destroy": true,
        "paging":   false,
        "ordering": true,
        //"sDom": '<"top"is>rt<"bottom"lp><"clear">',
        "info":     false
        } );
}



function addTableJs() {
    $(document).ready(function() {
        $('#table_quality').DataTable({
        "paging":   false,
        "ordering": true,
        "sDom": '<"top"i>rt<"bottom"lp><"clear">',
        "info":     false
    } );
    } );
}

/*
 * Добавляет обработку кнопки "В начало", что бы она появлялась и исчезала когда уходим и приходим из верхней позиции
 */
function addClickGoToTopButton() {
$(window).scroll(function() {
    if ($(this).scrollTop() > 250) {
        $('.go_top').stop(true, true).fadeIn();
    } else {
        $('.go_top').stop(true, true).fadeOut();
    }
});
}

function startOnLoad() {
drawPlots();
addTableJs();
addClickGoToTopButton();

}

$( document ).ready( startOnLoad);
