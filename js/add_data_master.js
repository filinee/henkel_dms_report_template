/*
 * Файл содержит мастер данные которые хранятся в глобальном контексте
 * 
 * */

month_name = [
{'long_ru': 'Январь', 'short_ru':'Янв', 'long_en': 'January', 'short_en':'Jan' },
{'long_ru': 'Февраль', 'short_ru':'Фев', 'long_en': 'February', 'short_en':'Feb' },
{'long_ru': 'Март', 'short_ru':'Март', 'long_en': 'March', 'short_en':'Mar' },
{'long_ru': 'Апрель', 'short_ru':'Апр', 'long_en': 'April', 'short_en':'Apr' },
{'long_ru': 'Май', 'short_ru':'Май', 'long_en': 'May', 'short_en':'May' },
{'long_ru': 'Июнь', 'short_ru':'Июнь', 'long_en': 'June', 'short_en':'June' },
{'long_ru': 'Июль', 'short_ru':'Июль', 'long_en': 'July', 'short_en':'July' },
{'long_ru': 'Август', 'short_ru':'Авг', 'long_en': 'August', 'short_en':'Aug' },
{'long_ru': 'Сентябрь', 'short_ru':'Сент', 'long_en': 'September', 'short_en':'Sep' },
{'long_ru': 'Октябрь', 'short_ru':'Окт', 'long_en': 'Oktober', 'short_en':'Okt' },
{'long_ru': 'Ноябрь', 'short_ru':'Нояб', 'long_en': 'November', 'short_en':'Nov' },
{'long_ru': 'Декабрь', 'short_ru':'Дек', 'long_en': 'December', 'short_en':'Dec' }
];

doc_type = {
    2:{name:"Продажи"},
    9:{name:"Возвраты"}
};


file_names = {
    pt: {name: 'period_tracker', descr: 'Отчет содержит данные о заказах обновленных в текущем периоде из закрытого периода'},
    dm: {name: 'data_date_map', descr: 'Отчет о полноте данных.'},
    vl: {name: 'validation', descr: 'Ошибки валидации данных.'},
    il: {name: 'info_loaded_packets', descr:'Данные о загруженных пакетах данных.'},
    rc: {name: 'reglament_controller', descr:'Данные по соблюдению регламента'},
    rcd: {name: 'reglament_controller_doctype', descr:'Данные по соблюдению регламента в разрезе документов'}
};
